<?php

function getSongs() {
  $songs = json_decode(file_get_contents("track.json"));
  return $songs === NULL ? [] : $songs;
}


if ($_SERVER["REQUEST_METHOD"] == "POST") {
  $songs = getSongs();
  $songs[] = json_decode($_POST["song"]);
  fwrite(fopen("track.json", "w"), json_encode($songs));
  echo "Song is saved to file";
} else if ($_SERVER["REQUEST_METHOD"] == "GET") {
  echo json_encode(getSongs());
}
