function renderEditTrackTable() {
  let noteHtml = "";
  for (let i = 0; i < 16; ++i) {
    noteHtml += `
      <div class="g_step" id="notes${i}">
        <div class="key_w" onclick="addNote('B', ${i}, this)"></div>
        <div class="key_b" onclick="addNote('A#', ${i}, this)"></div>
        <div class="key_w" onclick="addNote('A', ${i}, this)"></div>
        <div class="key_b" onclick="addNote('G#', ${i}, this)"></div>
        <div class="key_w" onclick="addNote('G', ${i}, this)"></div>
        <div class="key_b" onclick="addNote('F#', ${i}, this)"></div>
        <div class="key_w" onclick="addNote('F', ${i}, this)"></div>
        <div class="key_w" onclick="addNote('E', ${i}, this)"></div>
        <div class="key_b" onclick="addNote('D#', ${i}, this)"></div>
        <div class="key_w" onclick="addNote('D', ${i}, this)"></div>
        <div class="key_b" onclick="addNote('C#', ${i}, this)"></div>
        <div class="key_w" onclick="addNote('C', ${i}, this)"></div>
        <div class="key_b">${i + 1}</div>
      </div>      
    `;
  }  
  return `
  <div class="row justify-content-center">
    <div id="editNoteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false"
      class="modal fade text-left edit-note-modal-max edit-note-modal-min">
      <div role="document" class="modal-dialog">
        <div class="modal-content modal-track-edit">
          <div class="modal-body p-0">
            <button type="button" class="close" style="font-size: large;" data-dismiss="modal" 
              aria-hidden="true" onclick="closeEditNoteModal()">
              <i class="fas fa-times"></i>
            </button>
            <div class="row">
              <div class="col-2 label-notes" style="margin-top: 12px;">Notes</div>
              <div class="col-2 label-octave" style="margin-top: 12px;">Octave</div>
              <div class="col-4" style="margin-top: 4px;">
                <div class="row">
                  <div class="col-3 btn-track-a">
                    <button class="btn-octave active" onclick="changeOctave(2)">2</button>
                  </div>
                  <div class="col-3">
                    <button class="btn-octave" onclick="changeOctave(3)">3</button>
                  </div>
                  <div class="col-3">
                    <button class="btn-octave" onclick="changeOctave(4)">4</button>
                  </div>
                  <div class="col-3">
                    <button class="btn-octave" onclick="changeOctave(5)">5</button>
                  </div>
                </div>
              </div>
              <div class="col-3 btn-random-track" style="margin-top: 12px;">
                <span data-toggle="modal" data-target="#editRandomModal" onclick="setEditSectionRanges()">Random</span>
              </div>
            </div>
            <div class="row">
              <div class="col-12 notes-edit">
                <div id="noteMenu" class="">
                  <div class="edit_options label-edit">
                    <div id="keys">
                      <div class="key_w">B</div>
                      <div class="key_b">A#</div>
                      <div class="key_w">A</div>
                      <div class="key_b">G#</div>
                      <div class="key_w">G</div>
                      <div class="key_b">F#</div>
                      <div class="key_w">F</div>
                      <div class="key_w">E</div>
                      <div class="key_b">D#</div>
                      <div class="key_w">D</div>
                      <div class="key_b">C#</div>
                      <div class="key_w">C</div>
                      <div class="key_b2">Beat</div>
                    </div>
                    <div id="grid">
                      <div id="grid_steps">${noteHtml}</div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  `
}

function renderRandomModal() {
  return `
  <div class="row justify-content-center">
    <div id="editRandomModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"
      class="modal fade text-left modal-fullscreen" data-backdrop="static" data-keyboard="false">
      <div role="document" class="modal-dialog">
        <div class="modal-content modal-track-edit">
          <div class="modal-body p-0">
            <div class="row">
              <div class="col-3">
                <div id="randomSequenceEdit" class="edit_section">
                  <div class="section_title">Random Sequence</div>
                  <div class="slider">Scale [<span>Major</span>]
                    <div>
                      <input class="edit_section_range" value="1" type="range" min="1" max="6" oninput="updateRange(this, 'scale')">
                    </div>
                  </div>
                  <div class="slider">Root [<span>C</span>]
                    <div>
                      <input class="edit_section_range" value="1" type="range" min="1" max="12" oninput="updateRange(this, 'root')">
                    </div>
                  </div>
                  <div class="slider">Root Octave [<span>4</span>]
                    <div>
                      <input class="edit_section_range" value="4" type="range" min="0" max="7" oninput="updateRange(this, 'octave')">
                    </div>
                  </div>
                  <div class="slider">Max Steps [<span>8</span>]
                    <div>
                      <input class="edit_section_range" value="8" type="range" min="1" max="16" oninput="updateRange(this, 'maxNotes')">
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-7">
                <div class="row random-setting">
                  ${
                  `<div class="col-4">
                    <div class="select-dropdown instrument-item">
                      <select id=""></select>
                    </div>
                  </div>`.repeat(3)
                  }
                </div>
                <div class="row random-volume">
                  ${
                    `<div class="col-1">
                      <div class="volume-control">
                        <input class="volume" type="range" value="100" min="0" max="100" step="0.1">
                      </div>
                    </div>`.repeat(8)
                  }
                </div>
              </div>
              <div class="col-2 btn-random-setting">
                <button type="button" class="btn-gen-track" onclick="generateTrack(true)">Gen</button>
                <button type="button" class="btn-gen-track" onclick="clearNotes(true)">Clear</button>
                <button type="button" class="btn-gen-track" class="close" data-dismiss="modal" aria-hidden="true">Save</button>
              </div>
              <button id="btnCloseRandomModal" type="button" class="close" style="position: absolute; right: 0; font-size: large;" data-dismiss="modal" aria-hidden="true">
                <i class="fas fa-times"></i>
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  `;
}