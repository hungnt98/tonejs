/*
|--------------------------------------------------------------------------
| Global variables
|--------------------------------------------------------------------------
*/
var gScale = {
  major: ["C", "D", "E", "F", "G", "A", "B"],
  minor: ["C", "D", "Eb", "F", "G", "Ab", "Bb"],
  majorSeven: ["C", "E", "G", "B"],
  minorSeven: ["C", "Eb", "G", "Bb"],
  sus2: ["C", "D", "G"],
  sus4: ["C", "F", "G"]
}
var gMidiDevices = [];
var gCurrentTrack = gCurrentPart = 0;
var gNoTrack = 3;
var gEditOctave = 2;
var gTracks = [];
var gSongs = [];
var gInstruments = {
  'flute': Sampler.load({ instruments: "flute", ext: ".mp3" }),
  'piano': Sampler.load({ instruments: "piano", ext: ".mp3" }),
  'guitar-nylon': Sampler.load({ instruments: "guitar-nylon", ext: ".mp3" }),
  'guitar-acoustic': Sampler.load({ instruments: "guitar-acoustic", ext: ".mp3" }),
  'guitar-electric': Sampler.load({ instruments: "guitar-electric", ext: ".mp3" }),
  'bass-electric': Sampler.load({ instruments: "bass-electric", ext: ".mp3" }),
  'drums': new Tone.MembraneSynth(),
};
var gClickMusicFun = 0;
var gClickTempo = 0;
var gClickLoop = 0;
var gIsPlayingAll = false;
/*
|--------------------------------------------------------------------------
| Const HTML Element
|--------------------------------------------------------------------------
*/
const beatElements = [];
const btnCloseDeleteDialog = document.getElementById("btnCloseDeleteDialog");
const btnCloseNameSongModel = document.getElementById("btnCloseNameSongModel");
const btnCloseSongModel = document.getElementById("btnCloseSongModel");
const btnOctave = document.querySelectorAll(".btn-octave");
const btnTrackMenus = document.querySelectorAll(".btn-track-menu");
const editCheckBoxes = document.querySelectorAll(`input[name="editGroupCheckbox"]`);
const editInstrument = document.getElementById("editInstrument");
const editSectionRanges = document.querySelectorAll(".edit_section_range");
const editTrackName = document.getElementById("editTrackName");
const editVolume = document.getElementById("editVolume");
const musicFunMenu = document.getElementById("musicFunMenu");
const noteColumns = [];
const songContainer = document.getElementById("song-container")
const songName = document.getElementById("song-name");
const stepTime = document.getElementById("step-time");
const tempo = document.querySelector(".bpm");
const tempoModal = document.getElementById("tempoModal");
const loopModal = document.getElementById("loopModal");
const trackContainer = document.getElementById("track");
const trackInfs = [];


for (let i = 0; i < 16; ++i) {
  const noteElements = document.getElementById(`notes${i}`);
  noteColumns.push(noteElements);
  beatElements.push(noteElements.children[noteElements.childElementCount - 1]);
  beatElements.at(-1).style.fontWeight = "600";
  beatElements.at(-1).style.color = "#000";
}

editInstrument.addEventListener("change", (e) => {
  gTracks[gCurrentTrack].setInstrument(editInstrument.value);
})
editSectionRanges.forEach((section, index) => {
  if (index == 0) {
    section.addEventListener("input", (e) => {
      updateRange(section, "scale");
    })
  } else if (index == 1) {
    section.addEventListener("input", (e) => {
      updateRange(section, "root");
    })
  } else if (index == 2) {
    section.addEventListener("input", (e) => {
      updateRange(section, "octave");
    })
  } else {
    section.addEventListener("input", (e) => {
      updateRange(section, "maxNotes");
    })
  }
})
editTrackName.addEventListener("change", (e) => {
  gTracks[gCurrentTrack].setTrackName(editTrackName.value);
})
editVolume.addEventListener("input", (e) => {
  gTracks[gCurrentTrack].volumeElement.value = editVolume.value;
})
songName.addEventListener("keypress", (e) => {
  songName.style.border = "1px solid #cacaca";
  songName.nextElementSibling.innerHTML = "";
})
stepTime.addEventListener("change", (e) => {
  const step = parseInt(stepTime.value);
  for (let i = 0; i < 16; i++) {
    if (i % step === 0) {
      beatElements[i].style.fontWeight = "600";
      beatElements[i].style.color = "#000";
    } else {
      beatElements[i].style.fontWeight = "0";
      beatElements[i].style.color = "#a1a1a1";
    }
  }
  Tone.Transport.bpm.value = tempo.value * stepTime.value;
  playAll();
  playAll();
})

function activateTrack() {
  gNoTrack = parseInt(document.getElementById("quantity-track").value);
  for (let i = 0; i < 5; ++i) {
    if (i < gNoTrack) {
      trackInfs[i].style.opacity = 1;
      trackInfs[i].style.pointerEvents = "auto";
    } else {
      trackInfs[i].style.opacity = 0.25;
      trackInfs[i].style.pointerEvents = "none";
    }
  }
}

function addNote(note, step, button) {
  note = note + gEditOctave;
  const stepElementOnTrack = document.getElementById(`track${gCurrentTrack}`).children[step];
  const currentPart = gTracks[gCurrentTrack].getCurrentPart();
  // Add note if note exists then remove
  if (currentPart[step].addNote(note)) {
    button.innerHTML = note;
    button.style.backgroundColor = "var(--light-blue1)";
    button.style.color = "var(--dark-blue1)";
  } else {
    currentPart[step].removeNote(note);
    button.innerHTML = button.style.backgroundColor = button.style.color = "";
  }

  if (currentPart[step].notes.length == 0) {
    stepElementOnTrack.innerHTML = "";
  } else if (currentPart[step].notes.length > 0) {
    stepElementOnTrack.innerHTML = currentPart[step].notes.at(-1);
  }

  if (currentPart[step].notes.length > 1) {
    stepElementOnTrack.innerHTML = currentPart[step].notes.toString();
  }
}

function backToStart() {
  gTracks.forEach(track => {
    track.stop();
  });
  changePartAllTracks(0);
  gIsPlayingAll = false;
  beatElements.forEach(ele => ele.style.backgroundColor = "#fff");
  beatElements[0].style.backgroundColor = "#1cd6d6";
}

function clearNotes(renderToEditTable = false) {
  gTracks[gCurrentTrack].clear();
  if (renderToEditTable) {
    loadEditTable();
  }
}

function changeActivePart() {
  btnTrackMenus.forEach(btn => btn.classList.remove("active"));
  btnTrackMenus[gCurrentPart].classList.add("active");
}

function changeOctave(octave) {
  gEditOctave = octave;
  btnOctave.forEach(btn => btn.classList.remove("active"));
  btnOctave[octave - 2].classList.add("active");
}

function changePartAllTracks(partIndex) {
  gCurrentPart = partIndex;
  for (let i = 0; i < gNoTrack; ++i) {
    gTracks[i].setCurrentPart(partIndex);
  }
  changeActivePart();
}

function closeEditTrackModal() {
  $("#editTrackModal").modal('hide');
  loadEditTable();
}

function closeLoopMenu() {
  setTimeout(function () { loopModal.style.display = "none"; }, 5);
}

function closeModal() {
  $("#myModal").modal('hide');
  closeMusicFunMenu();
}

function closeMusicFunMenu() {
  setTimeout(function () { musicFunMenu.style.display = "none"; }, 5);
}

function closeTempoMenu() {
  setTimeout(function () { tempoModal.style.display = "none"; }, 5);
}

function deleteTrack() {
  // Store data before re-render
  const tracks = [];
  for (let i = 0; i < gNoTrack; ++i) {
    if (i != gCurrentTrack) {
      tracks.push(JSON.parse(JSON.stringify(gTracks[i].toObject())));
    }
  }
  // Change number of active tracks
  --gNoTrack;
  document.getElementById("quantity-track").value = gNoTrack;
  activateTrack();

  // Re-render
  renderTrack();
  for (let i = 0; i < gNoTrack; ++i) {
    gTracks[i].setData(tracks[i]);
  }

  btnCloseDeleteDialog.click();
  closeEditTrackModal();
}

function fetchSongs() {
  axios.get("filehandle.php")
    .then(response => gSongs.push(...response.data))
    .catch(error => console.error(error));
}

function generateTrack(renderToEditTable = false) {
  gTracks[gCurrentTrack].generate(renderToEditTable);
}

function init() {
  Tone.Transport.start();
  if (window.innerWidth < 1025) {
    document.getElementById("wrapper").style.zoom = "0.94";
  }
  const tab011 = document.getElementById("tab011");
  let html = '';
  for (let i = 0; i < 5; ++i) {
    html += `
    <tr>
      <th scope="row" class="label-track">Track ${i + 1}</th>
      <td><input id="trackName${i}" class="track-name" type="text" style="margin-left: 3vw;" value="" placeholder="Name"></td>
      <td>
        <div class="select-dropdown instrument-item" style="width: 88px; margin-left: 3vw;">
          <select id="trackInstrument${i}" >
            <option value="flute">Flute</option>
            <option value="piano" selected>Piano</option>
            <option value="guitar-nylon">Guitar Nylon</option>
            <option value="guitar-acoustic">Guitar Acoustic</option>
            <option value="guitar-electric">Guitar Electric</option>
            <option value="bass-electric">Bass</option>
            <option value="drums">Drums</option>
          </select>
        </div>
      </td>
      <td>
        <input class="checkbox-tempo mute" name="groupCheckBox${i}" type="checkbox">
      </td>
      <td>
        <input class="checkbox-tempo solo" name="groupCheckBox${i}" type="checkbox">
      </td>
      <td style="display: flex; justify-content: end;">
        <input type="range" class="form-range volume-ctrl" id="volume-track${i}" min="-40" max="30" step="0.1" value="0">
      </td>
    </tr>
  `;
  }
  tab011.innerHTML += `
  <div class="row">
    <div class="col-12">
      <table class="table-borderless">
        <thead>
          <tr>
            <th scope="col" style="padding-left: 9vw;"></th>
            <th scope="col" class="label-table-header" style="padding-left: 3vw;">Name</th>
            <th scope="col" class="label-table-header" style="padding-left: 3vw;">Instrument</th>
            <th scope="col" class="label-table-header" style="padding-left: 3vw;">Mute</th>
            <th scope="col" class="label-table-header" style="padding-left: 3vw;">Solo</th>
            <th scope="col" class="label-table-header" style="padding-left: 3vw;">Volume</th>
          </tr>
        </thead>
        <tbody>
          ${html}
        </tbody>
      </table>
    </div>
  </div>    
  `;
  const trackTable = document.querySelector(".table-borderless").children[1];
  for (let i = 0; i < trackTable.childElementCount; ++i) {
    trackInfs.push(trackTable.children[i]);
  }
  for (let i = 0; i < 5; ++i) {
    if (i < gNoTrack) {
      trackInfs[i].style.opacity = 1;
    } else {
      trackInfs[i].style.opacity = 0.25;
      trackInfs[i].style.pointerEvents = "none";
    }
  }
  for (let i = 0; i < 5; ++i) {
    if (i == 0) {
      gTracks.push(new Track(i, "Melody", "flute"));
    } else if (i == 1) {
      gTracks.push(new Track(i, "Chords", "piano"));
    } else if (i == 2) {
      gTracks.push(new Track(i, "Drums", "drums"));
    } else {
      gTracks.push(new Track(i));
    }
  }
  $("input:checkbox").on('click', function () {
    const $box = $(this);
    if ($box.is(":checked")) {
      const group = "input:checkbox[name='" + $box.attr("name") + "']";
      $(group).prop("checked", false);
      $box.prop("checked", true);
    } else {
      $box.prop("checked", false);
    }
  });
  fetchSongs();
}

function loadEditTable() {
  const trackPart = gTracks[gCurrentTrack].getCurrentPart();
  const stepElement = gTracks[gCurrentTrack].stepElement;
  const toIndex = (note) => {
    const notes = ["B", "A#", "A", "G#", "G", "F#", "F", "E", "D#", "D", "C#", "C"];
    return notes.indexOf(note.match(/[^\d]+/g)[0]);
  }
  // Iterate column of table
  for (let stepIndex = 0; stepIndex < 16; stepIndex++) {
    const noteElements = document.getElementById(`notes${stepIndex}`).children
    // Iterate row of table
    for (let i = 0; i < noteElements.length - 1; i++) {
      noteElements[i].innerHTML = "";
      noteElements[i].style.backgroundColor = "";
      noteElements[i].style.color = "";
    }
    trackPart[stepIndex].notes.forEach(note => {
      const index = toIndex(note);
      noteElements[index].innerHTML = note;
      noteElements[index].style.backgroundColor = "var(--light-blue1)";
      noteElements[index].style.color = "var(--dark-blue1)";
    })
  }
  let pos = -1;
  for (let i = 0; i < stepElement.childElementCount; ++i) {
    if (stepElement.style.borderTop == "1px solid white") {
      pos = i;
      break;
    }
  }
  beatElements[pos].style.backgroundColor = "#1cd6d6";
}

function loadSongs(demo = false) {
  closeMusicFunMenu();
  if (demo) {
    // load demo song here
    let html = "";
    gSongs.forEach((song, index) => html += `              
      <div class="col-3 label-song-name" type="button" onclick="openSong(${index})">
        <div class="song-name"">${index + 1}. ${song.name}</div>
      </div>`
    );
    songContainer.innerHTML = html;
  } else {
    let html = "";
    gSongs.forEach((song, index) => html += `              
      <div class="col-3 label-song-name" type="button" onclick="openSong(${index})">
        <div class="song-name"">${index + 1}. ${song.name}</div>
      </div>`
    );
    songContainer.innerHTML = html;
  }
}

function openLoopMenu() {
  closeMusicFunMenu();
  closeTempoMenu();
  setTimeout(function () { loopMenu.style.display = "block"; }, 5);
}

function openMusicFunMenu() {
  closeTempoMenu();
  closeLoopMenu();
  setTimeout(function () { musicFunMenu.style.display = "block"; }, 5);
}

function openSong(songIndex) {
  // Change value of active track
  gNoTrack = gSongs[songIndex].tracks.length;
  document.getElementById("quantity-track").value = gNoTrack;
  activateTrack();

  // Change tempo
  songTempo = gSongs[songIndex].tempo.split("x");
  document.querySelector(".bpm").value = songTempo[0];
  stepTime.value = songTempo[1];

  // Set name and instrument of each active track;
  renderTrack();
  for (let i = 0; i < gNoTrack; ++i) {
    gTracks[i].setData(gSongs[songIndex].tracks[i]);
  }
  btnCloseSongModel.click();
}

function openTempoMenu() {
  closeMusicFunMenu();
  closeLoopMenu();
  setTimeout(function () { tempoMenu.style.display = "block"; }, 5);
}

function playAll() {
  hasTrackPlayed = false;
  if (!gIsPlayingAll) {
    hasTrackSolo = gTracks.findIndex(track => track.isSolo()) != -1;
    gTracks.forEach(track => {
      if (track.canPlay(hasTrackSolo)) {
        track.startLoop();
        hasTrackPlayed = true;
      }
    })
  } else {
    gTracks.forEach(track => track.stop())
  }
  gIsPlayingAll = hasTrackPlayed;
}

function renderTrack() {
  $("#myModal").modal('hide');
  closeMusicFunMenu();
  gNoTrack = document.getElementById("quantity-track").value;
  trackContainer.innerHTML = "";
  for (let i = 0; i < gNoTrack; i++) {
    trackContainer.innerHTML += `
      <div class="track-card">
        <div class="row" style="margin-bottom: -8px;">
          <div class="col-6">
            <span type="button" data-toggle="modal" data-target="#trackNameModal" class="track-name-edit"
              onclick="updateTrackNameModel(${i})"></span>
          </div>
          <div class="col-6">
            <button data-toggle="modal" data-target="#editTrackModal" class="btn-update-track" 
              onclick="updateTrackModel(${i})">Update</button>
          </div>
        </div>
        <div class="row">
          <div class="col-12">
            <div id="tracks">
              <div class="track">
                <div class="track_steps" id="track${i}" style="margin-top: -16px;">
                  <div class="step step-bold" data-toggle="modal" data-target="#trackNameModal" onclick="updateTrackNameModel(${i})"></div>
                  <div class="step step-bold" data-toggle="modal" data-target="#trackNameModal" onclick="updateTrackNameModel(${i})"></div>
                  <div class="step step-bold" data-toggle="modal" data-target="#trackNameModal" onclick="updateTrackNameModel(${i})"></div>
                  <div class="step step-bold" data-toggle="modal" data-target="#trackNameModal" onclick="updateTrackNameModel(${i})"></div>
                  <div class="step step-nomal"></div>
                  <div class="step step-nomal"></div>
                  <div class="step step-nomal"></div>
                  <div class="step step-nomal"></div>
                  <div class="step step-bold"></div>
                  <div class="step step-bold"></div>
                  <div class="step step-bold"></div>
                  <div class="step step-bold"></div>
                  <div class="step step-nomal" data-toggle="modal" data-target="#editTrackModal" onclick="updateTrackModel(${i})"></div>
                  <div class="step step-nomal" data-toggle="modal" data-target="#editTrackModal" onclick="updateTrackModel(${i})"></div>
                  <div class="step step-nomal" data-toggle="modal" data-target="#editTrackModal" onclick="updateTrackModel(${i})"></div>
                  <div id="last" class="step step-nomal" data-toggle="modal" data-target="#editTrackModal" onclick="updateTrackModel(${i})"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    `;
  }
  const trackNameEdits = document.querySelectorAll(".track-name-edit");
  for (let i = 0; i < gNoTrack; ++i) {
    gTracks[i].clear();
    gTracks[i].setNameDisplay(trackNameEdits[i]);
    gTracks[i].setStepElement(document.getElementById(`track${i}`));
  }
  changePartAllTracks(0);
}

function saveToFile() {
  const isSafe = () => {
    if (gNoTrack == 0) {
      window.alert("Cannot save empty song");
      return false;
    }
    if (songName.value == "") {
      songName.style.border = "1px solid red";
      songName.nextElementSibling.innerHTML = "Please enter song name";
      return false;
    }
    return true;
  }
  if (isSafe()) {
    const data = {
      name: songName.value,
      tempo: `${tempo.value}x${stepTime.value}`,
      tracks: []
    }
    for (let i = 0; i < gNoTrack; ++i) {
      data.tracks.push(gTracks[i].toObject());
    }
    const formData = new FormData();
    formData.append("song", JSON.stringify(data));
    axios.post("filehandle.php", formData)
      .then(response => {
        window.alert(response.data);
        btnCloseNameSongModel.click();
      })
      .catch(error => console.error(error));
    gSongs.push(JSON.parse(JSON.stringify(data)));
  }
}

function selectLoop() {
  if (++gClickLoop % 2 !== 0) {
    openLoopMenu();
  } else {
    closeLoopMenu();
  }
}

function selectMusicFun() {
  if (++gClickMusicFun % 2 !== 0) {
    openMusicFunMenu();
  } else {
    closeMusicFunMenu();
  }
}

function selectTempo() {
  if (++gClickTempo % 2 !== 0) {
    openTempoMenu();
  } else {
    closeTempoMenu();
  }
}

function setEditSectionRanges() {
  updateRange(editSectionRanges[0], "scale", gTracks[gCurrentTrack].randScale);
  updateRange(editSectionRanges[1], "root", gTracks[gCurrentTrack].randRoot);
  updateRange(editSectionRanges[2], "octave", gTracks[gCurrentTrack].randOctave);
  updateRange(editSectionRanges[3], "maxNotes", gTracks[gCurrentTrack].randSteps);
}

function setMuteCurrentTrack() {
  gTracks[gCurrentTrack].setMute(editCheckBoxes[0].checked);
}

function setSoloCurrentTrack() {
  gTracks[gCurrentTrack].setSolo(editCheckBoxes[1].checked);
}

function closeDeleteTrackModal() {
  $("#deleteTrackModal").modal('hide');
}

function closeRandom() {
  $("#editRandomModal").modal('hide');
}

function setEditTrackModal(i) {
  setTimeout(
    function () {
      console.log($(window).width());
      if ($(window).width() >= 1024) {
        editNoteModal.style.marginTop = `${2 + (i+1)*10}vw`;
        editRandomModal.style.marginTop = `${32 + (i+1)*10}vw`;
        editNoteModal.style.position = "absolute";
        editRandomModal.style.position = "absolute";
        editNoteModal.style.height = "auto";
        editRandomModal.style.height = "auto";
      } else if ($(window).width() < 768) {
        editNoteModal.style.marginTop = `${16 + (i+1)*24}vw`;
        editRandomModal.style.marginTop = `${140 + (i+1)*24}vw`;
        grid_steps.style.width = "180vw";
        editNoteModal.style.position = "absolute";
        editRandomModal.style.position = "absolute";
        editNoteModal.style.height = "auto";
        editRandomModal.style.height = "auto";
      } else if (768 <= $(window).width() < 1024) {
        editNoteModal.style.marginTop = `${6 + (i+1)*18}vw`;
        editRandomModal.style.marginTop = `${66 + (i+1)*18}vw`;
        grid_steps.style.width = "90vw";
        editNoteModal.style.position = "absolute";
        editRandomModal.style.position = "absolute";
        editNoteModal.style.height = "auto";
        editRandomModal.style.height = "auto";
      } 
    }, 5);
}

function updateTrackModel(i) {
  gCurrentTrack = i;
  setEditTrackModal(i);
}

function tempoMenu() {
  closeMusicFunMenu();
  $("#loopModal").modal('hide');
  setTimeout(
    function () {
      if ($(window).width() >= 1024) {
        tempoModal.style.top = "1vh";
        tempoModal.style.height = "9vh";
        tempoModal.style.marginLeft = "4.8vw";
      } else if ($(window).width() < 768) {
        tempoModal.style.top = "3.2vh";
        tempoModal.style.height = "5vh";
        tempoModal.style.marginLeft = "17vw";
      } else if (768 <= $(window).width() < 1024) {
        tempoModal.style.top = "0.8vh";
        tempoModal.style.height = "6vh";
        tempoModal.style.marginLeft = "12vw";
      } 
    }, 5);
}

function loopMenu() {
  closeMusicFunMenu();
  $("#tempoModal").modal('hide');
  setTimeout(
    function () {
      if ($(window).width() >= 1024) {
        loopModal.style.top = "1vh";
        loopModal.style.height = "9vh";
        loopModal.style.marginLeft = "24.6vw";
      } else if ($(window).width() < 768) {
        loopModal.style.top = "3.2vh";
        loopModal.style.height = "5vh";
        loopModal.style.marginLeft = "37vw";
      } else if (768 <= $(window).width() < 1024) {
        loopModal.style.top = "0.8vh";
        loopModal.style.height = "6vh";
        loopModal.style.marginLeft = "33.5vw";
      } 
    }, 5);
}

function updateTrackNameModel(trackNum) {
  gCurrentTrack = trackNum;
  editTrackName.value = gTracks[gCurrentTrack].nameInput.value;
  editInstrument.value = gTracks[gCurrentTrack].instrumentSelector.value;
  editVolume.value = gTracks[gCurrentTrack].volumeElement.value;
  editCheckBoxes[0].checked = gTracks[gCurrentTrack].isMute();
  editCheckBoxes[1].checked = gTracks[gCurrentTrack].isSolo();
}

function updateRange(slider, type, val = null) {
  //Set parameters based on the sliders
  let value = 0;

  switch (type) {
    case "tempo":
      Tone.Transport.bpm.value = slider.value * stepTime.value;
      playAll();
      playAll();
      value = slider.value;
      break;
    case "clock":
      if (slider.value == 1) {
        value = "1 bar";
        gTracks[gCurrentTrack].clock = "1m";
      } else if (slider.value == 2) {
        value = "1/2";
        gTracks[gCurrentTrack].clock = "2n";
      } else if (slider.value == 3) {
        value = "1/4";
        gTracks[gCurrentTrack].clock = "4n";
      } else if (slider.value == 4) {
        value = "1/8";
        gTracks[gCurrentTrack].clock = "8n";
      } else if (slider.value == 5) {
        value = "1/16";
        gTracks[gCurrentTrack].clock = "16n";
      }

      gTracks[gCurrentTrack].sliderValues[2] = slider.value;
      break;
    case "noteTime":
      if (slider.value == 1) {
        value = "1 bar";
        gTracks[gCurrentTrack].noteTime = getStepTime("1m");
      } else if (slider.value == 2) {
        value = "1/2";
        gTracks[gCurrentTrack].noteTime = getStepTime("2n");
      } else if (slider.value == 3) {
        value = "1/4";
        gTracks[gCurrentTrack].noteTime = getStepTime("4n");
      } else if (slider.value == 4) {
        value = "1/8";
        gTracks[gCurrentTrack].noteTime = getStepTime("8n");
      } else if (slider.value == 5) {
        value = "1/16";
        gTracks[gCurrentTrack].noteTime = getStepTime("16n");
      } else if (slider.value == 6) {
        value = "1/32";
        gTracks[gCurrentTrack].noteTime = getStepTime("32n");
      }
      gTracks[gCurrentTrack].sliderValues[3] = slider.value;
      break;
    case "delay":
      value = slider.value * 4;
      gTracks[gCurrentTrack].cycleDelay = value;
      gTracks[gCurrentTrack].sliderValues[4] = slider.value;
      break;
    case "randSteps":
      if (slider.value == 1) {
        value = "off";
        gTracks[gCurrentTrack].cycleSteps = false;
      } else if (slider.value == 2) {
        value = "on";
        gTracks[gCurrentTrack].cycleSteps = true;
      }
      gTracks[gCurrentTrack].sliderValues[5] = slider.value;
      break;
    case "shiftNotes":
      if (slider.value == 1) {
        value = "off";
        gTracks[gCurrentTrack].shiftNotes = false;
      } else if (slider.value == 2) {
        value = "on";
        gTracks[gCurrentTrack].shiftNotes = true;
      }
      gTracks[gCurrentTrack].sliderValues[6] = slider.value;
      break;
    case "maxNotes":
      if (val != null) {
        slider.value = val;
      }
      value = slider.value;
      gTracks[gCurrentTrack].randSteps = value;
      gTracks[gCurrentTrack].sliderValues[10] = slider.value;
      slider.parentElement.previousElementSibling.innerHTML = value;
      break;
    case "octave":
      if (val != null) {
        slider.value = val;
      }
      value = slider.value;
      gTracks[gCurrentTrack].randOctave = value;
      gTracks[gCurrentTrack].sliderValues[9] = slider.value;
      slider.parentElement.previousElementSibling.innerHTML = value;
      break;
    case "tempo":
      value = slider.value;
      Tone.Transport.bpm.value = value;
      break;
    case "scale":
      if (val != null) {
        slider.value = val;
      }
      if (slider.value == 1) {
        value = "Major";
      } else if (slider.value == 2) {
        value = "Minor";
      } else if (slider.value == 3) {
        value = "Major7";
      } else if (slider.value == 4) {
        value = "Minor7";
      } else if (slider.value == 5) {
        value = "Sus2";
      } else if (slider.value == 6) {
        value = "Sus4";
      }
      gTracks[gCurrentTrack].randScale = slider.value;
      gTracks[gCurrentTrack].sliderValues[7] = slider.value;
      slider.parentElement.previousElementSibling.innerHTML = value;
      break;
    case "root":
      if (val != null) {
        slider.value = val;
      }
      console.log(slider.value);
      if (slider.value == 1) {
        value = "C";
      } else if (slider.value == 2) {
        value = "C#";
      } else if (slider.value == 3) {
        value = "D";
      } else if (slider.value == 4) {
        value = "D#";
      } else if (slider.value == 5) {
        value = "E";
      } else if (slider.value == 6) {
        value = "F";
      } else if (slider.value == 7) {
        value = "F#";
      } else if (slider.value == 8) {
        value = "G";
      } else if (slider.value == 9) {
        value = "G#";
      } else if (slider.value == 10) {
        value = "A";
      } else if (slider.value == 11) {
        value = "A#";
      } else if (slider.value == 12) {
        value = "B";
      }
      gTracks[gCurrentTrack].randRoot = slider.value - 1;
      gTracks[gCurrentTrack].sliderValues[8] = slider.value;
      slider.parentElement.previousElementSibling.innerHTML = value;
      break;
    case "channel":
      value = slider.value;
      gTracks[gCurrentTrack].midiChannel = value;
      gTracks[gCurrentTrack].sliderValues[1] = slider.value;
      break;
    case "device":
      if (gMidiDevices.length < 1) {
        value = "No Devices";
      } else {
        value = gMidiDevices[slider.value - 1].name;
        gTracks[gCurrentTrack].midiDevice = slider.value - 1;
        gTracks[gCurrentTrack].sliderValues[0] = slider.value;
      }
      break;
    default:
      value = slider.value;
      break;
  }
  slider.parentElement.children[0].innerHTML = value;
}

function updateTrackInstrument() {
  gTracks[gCurrentTrack].setInstrument(editInstrument.value, true);
}
