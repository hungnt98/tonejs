class Step {
  constructor() {
    this.notes = [];
  }

  /**
   * Add note to step. If note exists do nothing and returns false
   * @param {string} note 
   */
  addNote(note) {
    const isExist = this.notes.includes(note);
    if (!isExist) {
      this.notes.push(note);
    }
    return !isExist;
  }

  /**
   * Make step empty
   */
  clear() {
    this.notes.length = 0;
  }

  /**
   * Remove specify note from step
   * @param {string} note 
   */
  removeNote(note) {
    const index = this.notes.findIndex(n => n == note);
    if (index != -1) {
      this.notes.splice(index, 1);
    }
  }

  /**
   * 
   * @param {object} step 
   */
  setStep(step) {
    this.notes = [...step.notes];
  }

  toString() {
    return this.notes.join(" ");
  }
}

class Track {
  /**
   * Construct track object
   * @param {int} trackNum 
   * @param {string} trackName
   * @param {string} instrument
   */
  constructor(trackNum, trackName = null, instrument = null) {
    /**
     * Track HTML Elements
     */
    this.nameInput = document.getElementById(`trackName${trackNum}`);
    this.nameDisplay = null;
    this.instrumentSelector = document.getElementById(`trackInstrument${trackNum}`);
    this.stepElement = null;
    this.volumeElement = document.getElementById(`volume-track${trackNum}`);
    this.checkBoxes = document.querySelectorAll(`input[name="groupCheckBox${trackNum}"]`);
    this.nameInput.value = trackName ? trackName : "";
    this.instrumentSelector.value = instrument ? instrument : "piano";

    this.parts = [
      Array(16).fill().map(ele => new Step()),
      Array(16).fill().map(ele => new Step()),
      Array(16).fill().map(ele => new Step()),
      Array(16).fill().map(ele => new Step())
    ];
    this.currentPart = 0;
    this.currentStep = null;
    this.loop = null; // Interval
    this.trackNum = trackNum; // Track number

    this.midiDevice = 0;
    this.midiChannel = 1;
    this.clock = "8n";
    this.stepTime = this.noteTime = getStepTime("8n");
    this.cycleDelay = 0;
    this.cycleSteps = false;
    this.shiftNotes = false;

    this.randScale = 1;
    this.randRoot = 0;
    this.randOctave = 4;
    this.randSteps = 8;

    this.sliderValues = [1, 1, 4, 3, 0, 1, 1, 0, 0, 4, 8];
  }

  /**
   * Check if track can play
   * @param {boolean} hasTrackSolo 
   */
  canPlay(hasTrackSolo) {
    if (this.trackNum >= gNoTrack) {
      return false;
    }
    if (hasTrackSolo) {
      return !this.loop && this.checkBoxes[1].checked && !this.empty();
    }
    return !this.loop && !this.checkBoxes[0].checked && !this.empty();
  }

  /**
   * Clear notes in track
   * @param {boolean} all
   */
  clear(all=false) {
    if (all) {
      this.parts.forEach(part => part.forEach(step => step.clear()));
    } else {
      this.parts[this.currentPart].forEach(step => step.clear());
    }
    if (this.stepElement != null) {
      for (let i = 0; i < this.stepElement.childElementCount; ++i) {
        this.stepElement.children[i].innerHTML = "";
      }
    }
  }

  /**
   * Check if track is empty
   * @param {number} partIndex
   */
  empty(partIndex = null) {
    partIndex = partIndex != null ? partIndex : this.currentPart;
    return this.parts[partIndex].findIndex(step => step.notes.length != 0) == -1;
  }

  /**
   * Check if track is muted
   */
  isMute() {
    return this.checkBoxes[0].checked;
  }

  /**
   * Check if track is playing
   */
  isPlaying() {
    return !!this.loop;
  }

  /**
   * Check if track is muted
   */
  isSolo() {
    return this.checkBoxes[1].checked;
  }

  /**
   * Randomize notes in track
   * @param {boolean} renderToEditTable
   */
  generate(renderToEditTable=false) {
    gCurrentTrack = this.trackNum;
    const scaleType = () => {
      if (this.randScale == 1)
        return "major";
      if (this.randScale == 2)
        return "minor";
      if (this.randScale == 3)
        return "majorSeven";
      if (this.randScale == 4)
        return "minorSeven";
      if (this.randScale == 5)
        return "sus2";
      return "sus4";
    }
    const pool = gScale[scaleType()].map(scale =>
      Tone.Frequency(scale + (Number(this.randOctave))).transpose(this.randRoot).toNote()
    );
    if (this.currentStep == null) {
      this.currentStep = 0;
      this.stepElement.children[this.currentStep].style.borderTop = "1px solid white";
      this.stepElement.children[this.currentStep].style.borderBottom = "1px solid white";
    }
    // Generate sequence
    for (let i = 0; i < 16; i++) {
      this.parts[this.currentPart][i].notes.length = 0;
      const noteIndex = getRandom(0, pool.length);
      this.parts[this.currentPart][i].notes.push(pool[noteIndex]);
      this.stepElement.children[i].innerHTML = pool[noteIndex];
      this.stepElement.children[i].style.backgroundColor = "";
    }
    if (renderToEditTable) {
      loadEditTable();
    }
  }

  /**
   * Return current part in track
   */
  getCurrentPart() {
    return this.parts[this.currentPart];
  }

  /**
   * Return track name and its instrument
   * @returns {string}
   */
  getTrackName() {
    return this.nameInput.value ? `${this.nameInput.value} - ${this.instrumentSelector.value}` : `Track ${this.trackNum + 1}`;
  }

  /**
   * Change current part of track
   * @param {number} partIndex 
   * @param {boolean} changeActive
   */
  setCurrentPart(partIndex, changeActive=false) {
    this.currentPart = partIndex;
    this.currentStep = 0;
    if (this.stepElement != null) {
      for (let i = 0; i < this.stepElement.childElementCount; ++i) {
        if (i == 0) {
          this.stepElement.children[i].style.borderTop = "1px solid white";
          this.stepElement.children[i].style.borderBottom = "1px solid white";
        } else {
          this.stepElement.children[i].style.borderTop = "";
          this.stepElement.children[i].style.borderBottom = "";
        }
        this.stepElement.children[i].innerHTML = this.parts[this.currentPart][i].toString();
      }
    }
    if (changeActive && this.currentPart != gCurrentPart) {
      gCurrentPart = this.currentPart;
      changeActivePart();
    }
  }

  /**
   * Change instrument
   * @param {string} instrument
   * @param {boolean} updateDisplay
   */
  setInstrument(instrument, updateDisplay = false) {
    this.instrumentSelector.value = instrument;
    if (updateDisplay) {
      this.nameDisplay.innerHTML = this.getTrackName();
    }
  }

  /**
   * Set mute of track
   * @param {boolean} mute
   */
  setMute(mute) {
    this.checkBoxes[0].checked = mute;
    this.checkBoxes[1].checked = this.checkBoxes[0].checked ? false : this.checkBoxes[1].checked;
  }

  /**
   * Change HTMLElement display of track
   * @param {HTMLElement} element
   */
  setNameDisplay(element) {
    this.nameDisplay = element;
    this.nameDisplay.innerHTML = this.getTrackName();
  }

  /**
   * Set solo of track
   * @param {boolean} solo
   */
  setSolo(solo) {
    this.checkBoxes[1].checked = solo;
    this.checkBoxes[0].checked = this.checkBoxes[1].checked ? false : this.checkBoxes[0].checked;
  }

  /**
   * @param {HTMLElement} element
   */
  setStepElement(element) {
    this.stepElement = element;
  }

  /**
   * @param {object} data 
   */
  setData(data) {
    for (let i = 0; i < this.parts.length; ++i) {
      for (let j = 0; j < this.parts[i].length; ++j) {
        this.parts[i][j].setStep(data.parts[i][j]);
      }
    }
    this.setInstrument(data.instrument);
    this.setTrackName(data.name);
    this.setCurrentPart(0);
  }

  /**
   * Change track name
   * @param {string} name
   */
  setTrackName(name) {
    this.nameInput.value = name;
    this.nameDisplay.innerHTML = this.getTrackName();
  }

  /**
   * Play track
   */
  startLoop() {
    // Get parameters to use in our loop
    const track = this;
    this.stepTime = getStepTime(this.clock);

    // Start the sequence    
    this.loop = setInterval(function () {
      const canPerform = () => {
        const hasTrackSolo = gTracks.findIndex(track => track.isSolo()) != -1;
        return !track.isMute() && (hasTrackSolo ? track.isSolo() : true);
      }

      // Handle current step
      if (track.currentStep < track.stepElement.childElementCount) {
        // Change css of step element
        if (track.currentStep > 0) {
          track.stepElement.children[track.currentStep - 1].style.border = "";
        }
        track.stepElement.children[track.currentStep].style.borderTop = "1px solid white";
        track.stepElement.children[track.currentStep].style.borderBottom = "1px solid white";
        if (track.trackNum == gCurrentTrack) {
          if (track.currentStep > 0) {
            beatElements[track.currentStep - 1].style.backgroundColor = "#fff";
          }
          beatElements[track.currentStep].style.backgroundColor = "#1cd6d6"
        }
      } else {
        // Move to next part of track
        const nextPartIndex = track.currentPart + 1 < 4 ? track.currentPart + 1 : 0;
        track.currentPart = !track.empty(nextPartIndex) ? nextPartIndex : 0;
        track.setCurrentPart(track.currentPart, true);
        if (track.trackNum == gCurrentTrack) {
          beatElements.at(-1).style.backgroundColor = "#fff";
          beatElements[0].style.backgroundColor = "#1cd6d6"
        }
      }

      // Play instrument
      if (canPerform()) {        
        track.parts[track.currentPart][track.currentStep].notes.forEach(note => {
          const instrument = track.instrumentSelector.value;
          const volumeVal = track.volumeElement.value;
          gInstruments[instrument].volume.value = volumeVal == -40 ? -100 : volumeVal;
          gInstruments[instrument].toMaster();
          gInstruments[instrument].triggerAttackRelease(note, track.clock);
        });
      }

      ++track.currentStep;
    }, this.stepTime);
  }

  /**
   * Stop track
   * @param {boolean} reset 
   */
  stop() {
    // If track is played, stop track
    if (this.isPlaying()) {
      clearInterval(this.loop);
      this.loop = null;
    }
  }

  /**
   * Convert track to special type before save to file
   */
  toObject() {
    const data = {
      name: this.nameInput.value,
      instrument: this.instrumentSelector.value,
      parts: []
    };
    data.parts.push(...this.parts);
    return data;
  }
}



// Get note length in milliseconds
function getStepTime(toneTime) {
  return Tone.Time(toneTime).toSeconds() * 1000;
}

function getRandom(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min;
}